/* THIS CODE IS A COMPLETELY UNTESTED MOCK-UP */
#ifndef _STR_H
#define _STR_H

/* This header provides better strings for C99. */

#include <assert.h>

/* you can define MALLOC, CALLOC, FRE and REALLOC to your own function to
 * override. */
#ifdef CALLOC
#define calloc CALLOC
#endif

#ifdef MALLOC
#define malloc MALLOC
#endif

#ifdef REALLOC
#define realloc REALLOC
#endif

#ifdef FREE
#define free FREE
#endif

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
	size_t len; /* string length in bytes, minus trailing nul */
	size_t size; /* object size of this string */
	bool is_const; /* is the pointer pointed to by str a constant? */
	char *str; /* the pointer */
} String;

/* This function/macro is used to turn a C string literal into a String */
static inline String S(char *str, size_t size) {
	String s = { strlen(str), size, true, str };
	return s;
}
#define S(s) S((char*)(s), sizeof(s)) 
#define _S_INIT {0,0,0,0}

static inline size_t Strlen(String s)
{
	return s.len;
}

/* Free space at the end of string. */
static inline size_t Strspace(String s)
{
	/* reserve 1 byte for trailing NUL */
	return s.size - s.len - 1;
}

static void Strcat(String dest, String source)
{
	assert(!dest.is_const);
	if (Strspace(dest) < Strlen(source)) {

			assert(false); /* TODO realloc */

		
	}
	memcpy(dest.str + Strlen(dest), source.str, source.len);
	dest.len += source.len;
	dest.str[dest.len] = '\0';
}

static String Strdup(String src)
{
	String dest = _S_INIT;
	dest.str = malloc(src.len + 1);
	assert(dest.str); /* FIXME */
	dest.is_const = false;
	dest.len = src.len;
	dest.size = src.len + 1;
	memcpy(dest.str, src.str, dest.size);
	return dest;
}


#endif
